﻿using System;
using System.Reflection;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container.Impl.Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Main.Contract.IMain));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Main.Implementation.CMain));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(Display.Contract.IDisplay));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Display.Implementation.Display));

        #endregion
    }
}
