﻿using System;
using PK.Container;
using Container.Impl;
namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            var container = new Container.Impl.Container();
            container.Register(typeof(void));
            return container;
        }
    }
}
